import { doc, getDoc, setDoc } from 'firebase/firestore';
import type { Unsubscribe } from 'firebase/firestore';
import { db } from './firebase';
import { type UrlMapped } from '~/utils/types';

export async function getUrl(
    urlId: string): Promise<UrlMapped> {
    const docRef = doc(db, 'urls', urlId);
    const docSnap = await getDoc(docRef)
    const info = { ...docSnap.data() } as UrlMapped

    return info;
}

export async function setUrl(
    urlId: string,
    urlMappedObj: UrlMapped,
): Promise<boolean> {
    const docRef = doc(db, 'urls', urlId);

    let response = false
    try {
        await setDoc(docRef, urlMappedObj)
        response = true
    } catch (error) {
        console.log(error)
    }

    return response;
}