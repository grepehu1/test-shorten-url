import Head from "next/head";

export default function NotFound() {
    return (
        <>
            <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-b from-[#2e026d] to-[#15162c]">
                404 - Not Found
            </main>
        </>
    );
}
