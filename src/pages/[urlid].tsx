import { getUrl } from "~/server/functions";

export const getServerSideProps = (async (context) => {
    const urlId = context?.query?.urlid as string

    const urlObj = await getUrl(urlId)

    if (Object.keys(urlObj).length) {
        return {
            redirect: {
                permanent: false,
                destination: urlObj.trueUrl
            }
        }
    } else {
        return {
            notFound: true
        }
    }

})

export default function UrlRedirect() {

    return (
        <></>
    );
}