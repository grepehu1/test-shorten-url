export type UrlMapped = {
    trueUrl: string;
    mappedUrl: string;
}