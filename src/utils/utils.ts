import { getUrl } from "~/server/functions";

export function generateNewId(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}

export async function generateUniqueId() {
    let newUrlId = generateNewId(6)
    let alreadyExists = await getUrl(newUrlId)
    while (Object.keys(alreadyExists).length) {
        newUrlId = generateNewId(6)
        alreadyExists = await getUrl(newUrlId)
    }
    return newUrlId
}